#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gmp.h>

// mpz_t MCD (mpz_t a, mpz_t b){
//   mpz_t c;
//   while ( a != 0 ) {
//      c = a;
//      a = b%a;
//      b = c;
//   }
//   return b;
// }

void cypher (mpz_t res, mpz_t m, mpz_t e, mpz_t n){
    //res = pow(m,e);
    //res = res%n;
    mpz_powm(res, m, e, n);
}

void unCypher (mpz_t res, mpz_t c, mpz_t d, mpz_t n){
    // res = pow(c,d);
    // res = res % n;
    mpz_powm(res, c, d, n);
}

int primo(mpz_t num) {
    unsigned long int i, aux;
    mpz_t j;
    mpz_init(j);

    mpz_sqrt(j, num);
    //for(i=2;i<=j;i++) {
    while(mpz_cmp_si(j,i) >= 0){
    	aux = mpz_fdiv_ui(num, i);
        if(aux == 0){
            return 0;
        }
        i++;
    }
    return 1;
}

int main() {
	mpz_t p;
	mpz_t q;
	mpz_t e;
    mpz_t phi, n;
    mpz_t i, d;
    mpz_t m, c;
    int opc, P, Q;
    char caracter;

    mpz_init(p);
    mpz_init(q);
    mpz_init(e);
    mpz_init(phi);
    mpz_init(n);
    mpz_init(i);
    mpz_init(d);
    mpz_init(m);
    mpz_init(c);

	//gmp_scanf("%Zd", p);
	scanf("%d%c", &P, &caracter);
	mpz_set_ui(p, P);
	

	// gmp_scanf("%Zd", q);
	scanf("%d%c", &Q, &caracter);
	mpz_set_ui(q, Q);
	

    //if(!primo(p) || !primo(q)){
	if(mpz_probab_prime_p(p, 20) == 0 || mpz_probab_prime_p(q, 20) == 0){
        printf("Los numeros no son primos\n");
        exit(1);
    }

	//n = p*q;
	mpz_mul(n, p, q);

	//phi = (p-1) * (q-1);
	mpz_t aux1, aux2, aux3;
	mpz_init(aux1);
	mpz_init(aux2);
	mpz_init(aux3);

	mpz_sub_ui(aux1, p, 1);
	mpz_sub_ui(aux2, q, 1);
	mpz_mul(phi, aux1, aux2);

    gmp_printf("p = %Zd, q = %Zd\n", p, q);
    gmp_printf("n = %Zd\n", n);
	gmp_printf("PHI = %Zd\n", phi);

    // for (i = 2; i < phi; i++){
	mpz_set_ui(i, 2);
	while(mpz_cmp(i, phi) < 0){
        //if(i%2 != 0){
		if(mpz_fdiv_ui(i, 2) != 0){
            //if( (phi % i != 0) && (n % i != 0) ){
			mpz_mod(aux1, phi, i);
			mpz_mod(aux2, n, i);
			if((mpz_sgn(aux1) != 0)&&(mpz_sgn(aux2) != 0)){
                mpz_set(e, i);
                //if(MCD(e,phi) == 1){
                mpz_gcd(aux3, e, phi);
                if(mpz_cmp_si(aux3, 1) == 0){
                    break;
                }
            }
        }
        mpz_add_ui(i, i, 1);
    }

    gmp_printf("\ne = %Zd", e);

    //for (i = 1; ; i++){
    mpz_set_ui(i, 1);
    while(1){
        //if ((e*i)%phi == 1){
    	mpz_mul(aux1, e, i);
    	mpz_mod(aux2, aux1, phi);
    	if(mpz_cmp_si(aux2, 1) == 0){
            mpz_set(d,i);
            break;
        }
        mpz_add_ui(i, i, 1);
    }

    gmp_printf("\nd = %Zd", d);

    gmp_printf("\nPublic KEY = (%Zd,%Zd)", e,n);
    gmp_printf("\nPrivate KEY = (%Zd,%Zd)", d,n);

    gmp_printf("\n1) Cifrar\n2) Descifrar\n");
    scanf("%d",&opc);
    switch(opc){
        case 1:
            printf("\n\nEscribe tu mensaje a cifrar: ");
            gmp_scanf("%Zd", m);

            cypher(c, m, e, n); //mensaje a cifrar & llave publica

            gmp_printf("\nMensaje cifrado = %Zd\n", c);
            break;
        case 2:
            printf("\n\nEscribe tu mensaje a cifrado: ");
            gmp_scanf("%Zd", c);
            printf("\nEscribe tu llave privada\nd: ");
            gmp_scanf("%Zd", d);
            printf("\nn: ");
            gmp_scanf("%Zd", n);

            unCypher(m, c, d, n); //mensaje cifrado & llave privada

            gmp_printf("\nMensaje descifrado = %Zd\n", m);
            break;
    }

}
