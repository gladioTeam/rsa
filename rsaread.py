import random;
import math;

def isPrime(num):
    i = 2
    j = int(math.sqrt(num));
    for i in range(2,j):
    	if(num % i == 0):
    		return 0;

    return 1;



def MCD(a,  b):
	while(a != 0):
		c = a;
		a = b %a;
		b = c;
	return b;


def cypher( m,  e,  n ):
	res = pow(m, e);
	res = res %n;
	return res;

def uncypher( c, d,  n):
	res = pow(c,d);
	res = res %n;
	return res;


while(1):
	print("Escribe  tu mensaje ó -1 si deseas salir")
	message = int(input());
	if message == -1:
		break;
	print("Escribe el valor de p");
	p = int(input());
	print("Escribe el valor de q")
	q = int(input());
	print("p: " + str(p));
	print("q: " + str(q));
	if(isPrime(p) and isPrime(q)):
		n = p * q;
		phi = (p-1) * (q-1);
		i = 2;

		for i in range(phi):
			if(i%2 != 0):
				if( (phi % i != 0) and (n % i != 0) ):
					e = i;
					if(MCD(e, phi) == 1):
						break;

		print("e= " + str(e));

		i = 1;

		while(1):
			if((e * i) % phi == 1):
				d = i
				break;
			i = i+1;

		print("d = " + str(d));

		print("public= "+str(e) + "," + str(n) );
		print("private ="+ str(d) + "," + str(n));

		cif = cypher(message, e,n);
		uncif = uncypher(cif,d,n);

		print("Mensaje cifrado: " + str(cif));
		print("Mensaje descifrado: " + str(uncif));
	else:
		print("P y Q no son primos");