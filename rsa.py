import random;

def rand_prime():
    while True:
        p = random.randrange(1, 1000, 2)
        if all(p % n != 0 for n in range(3, int((p ** 0.5) + 1), 2)):
            return p


def MCD(a,  b):
	while(a != 0):
		c = a;
		a = b %a;
		b = c;
	return b;


def cypher( m,  e,  n ):
	res = pow(m, e);
	res = res %n;
	return res;

def uncypher( c, d,  n):
	res = pow(c,d);
	res = res %n;
	return res;

print("Escribe  tu mensaje o 0 si deseas salir")

while(1):
	message = int(input());
	if message == 0:
		break;

	p = rand_prime();
	q = rand_prime();
	print("p: " + str(p));
	print("q: " + str(q));
	n = p * q;
	phi = (p-1) * (q-1);
	i = 2;

	for i in range(phi):
		if(i%2 != 0):
			if( (phi % i != 0) and (n % i != 0) ):
				e = i;
				if(MCD(e, phi) == 1):
					break;

	print("e= " + str(e));

	i = 1;

	while(1):
		if((e * i) % phi == 1):
			d = i
			break;
		i = i+1;

	print("d = " + str(d));

	print("public= "+str(e) + "," + str(n) );
	print("private ="+ str(d) + "," + str(n));

	cif = cypher(message, e,n);
	uncif = uncypher(cif,d,n);

	print("Mensaje cifrado: " + str(cif));
	print("Mensaje descifrado: " + str(uncif));